/*
Copyright 2012 Jun Wako <wakojun@gmail.com>
Copyright 2015 Jack Humbert

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once

#define PRODUCT Tractyl Manuform(5x6) BlackPill

// wiring of each half
#define MATRIX_COL_PINS \
    { B3, B4, B5, C13, C14, C15, A13 }
#define MATRIX_ROW_PINS \
    { A8, B15, B14, B13, B12, A15 }

#define UNUSED_PINS \
    { A10 }

#define DIODE_DIRECTION     COL2ROW

// #define USB_VBUS_PIN        B10 // doesn't seem to work for me on one of my controllers... */
//#define SPLIT_HAND_PIN      C14  // high = left, low = right
// COL7, ROW4 split hand
#define SPLIT_HAND_MATRIX_GRID B13, A13


/* Audio config */
#define AUDIO_PIN          B1
#define AUDIO_PWM_DRIVER   PWMD3
#define AUDIO_PWM_CHANNEL  4
#define AUDIO_PWM_PAL_MODE 2
#define AUDIO_STATE_TIMER  GPTD4

#ifdef AUDIO_ENABLE
#    define STARTUP_SONG SONG(STARTUP_SOUND)
#endif

/* serial.c configuration for split keyboard */
#define SERIAL_USART_FULL_DUPLEX  // Enable full duplex operation mode.
#define SERIAL_USART_TX_PIN      A2
#define SERIAL_USART_RX_PIN      A3
#define SERIAL_USART_DRIVER      SD2
#define SERIAL_USART_TX_PAL_MODE 7    // Pin "alternate function", see the respective datasheet for the appropriate values for your MCU. default: 7
#define SERIAL_USART_RX_PAL_MODE 7    // Pin "alternate function", see the respective datasheet for the appropriate values for your MCU. default: 7
#define SERIAL_USART_TIMEOUT     10  // USART driver timeout. default 100

#define CRC8_USE_TABLE
#define CRC8_OPTIMIZE_SPEED

/* i2c config for oleds */
#define I2C_DRIVER        I2CD1
#define I2C1_SCL_PIN      B8
#define I2C1_SDA_PIN      B9
#define I2C1_SCL_PAL_MODE 4
#define I2C1_SDA_PAL_MODE 4
#define I2C1_CLOCK_SPEED  400000
#define I2C1_DUTY_CYCLE FAST_DUTY_CYCLE_2

/* encoder config */
//bool encoder_update_kb(uint8_t index, bool clockwise) {
//    return encoder_update_user(index, clockwise);
//}

/* spi config for eeprom and pmw3360 sensor */
#define SPI_DRIVER                           SPID1
#define SPI_SCK_PIN                          A5
#define SPI_SCK_PAL_MODE                     5
#define SPI_MOSI_PIN                         A7
#define SPI_MOSI_PAL_MODE                    5
#define SPI_MISO_PIN                         A6
#define SPI_MISO_PAL_MODE                    5

/* eeprom config */
#define EXTERNAL_EEPROM_SPI_SLAVE_SELECT_PIN A4
#define EXTERNAL_EEPROM_SPI_CLOCK_DIVISOR 64  // Max. is 25 MHz, Djin has (160MHz/8) => 20MHz, this one has (96 MHz/4) = 24 MHz
#define EXTERNAL_EEPROM_BYTE_COUNT 8192
#define EXTERNAL_EEPROM_PAGE_SIZE 64  // it's FRAM, so it doesn't actually matter, this just sets the RAM buffer

// External flash config
#define EXTERNAL_FLASH_SPI_SLAVE_SELECT_PIN A0
#define EXTERNAL_FLASH_SPI_CLOCK_DIVISOR 64           // Max. is 133MHz, maybe the eeprom is the limit (160MHz/8) => 20MHz
#define EXTERNAL_FLASH_BYTE_COUNT (4 * 1024 * 1024)  // 32Mb/4MB capacity
#define EXTERNAL_FLASH_PAGE_SIZE (4 * 1024)          // 4kB pages

/* pmw3389 config  */
#define PMW3389_CS_PIN                       A14
#define PMW3389_SPI_MODE                     3
#define PMW3389_SPI_DIVISOR                  64 // Max. 2 Mhz. (96 Mhz/64) = 1.5 MHz 
#define PMW3389_FIRMWARE_UPLOAD_FAST

#define STM32_HSECLK 8000000
