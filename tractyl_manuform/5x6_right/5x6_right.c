/* Copyright 2020 Christopher Courtney, aka Drashna Jael're  (@drashna) <drashna@live.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "5x6_right.h"

#ifdef SWAP_HANDS_ENABLE
const keypos_t PROGMEM hand_swap_config[MATRIX_ROWS][MATRIX_COLS] = {
    /* Left hand, matrix positions */
    {{5, 6}, {4, 6}, {3, 6}, {2, 6}, {1, 6}, {0, 6}},
    {{5, 7}, {4, 7}, {3, 7}, {2, 7}, {1, 7}, {0, 7}},
    {{5, 8}, {4, 8}, {3, 8}, {2, 8}, {1, 8}, {0, 8}},
    {{5, 9}, {4, 9}, {3, 9}, {2, 9}, {1, 9}, {0, 9}},
    {{5, 10}, {4, 10}, {3, 10}, {2, 10}, {1, 10}, {0, 10}},
    {{5, 11}, {4, 11}, {3, 11}, {2, 11}, {1, 11}, {0, 11}},
    /* Right hand, matrix positions */
    {{5, 0}, {4, 0}, {3, 0}, {2, 0}, {1, 0}, {0, 0}},
    {{5, 1}, {4, 1}, {3, 1}, {2, 1}, {1, 1}, {0, 1}},
    {{5, 2}, {4, 2}, {3, 2}, {2, 2}, {1, 2}, {0, 2}},
    {{5, 3}, {4, 3}, {3, 3}, {2, 3}, {1, 3}, {0, 3}},
    {{5, 4}, {4, 4}, {3, 4}, {2, 4}, {1, 4}, {0, 4}},
    {{5, 5}, {4, 5}, {3, 5}, {2, 5}, {1, 5}, {0, 5}}};

#    ifdef ENCODER_MAP_ENABLE
const uint8_t PROGMEM encoder_hand_swap_config[NUM_ENCODERS] = {1, 0};
#    endif
#endif

const is31_led PROGMEM g_is31_leds[DRIVER_LED_TOTAL] = {
    { 0, G_16, H_16, I_16  }, //0
    { 0, E_16, F_16, D_16  }, //1
    { 0, E_15, D_15, F_15  }, //2
    { 0, D_14, E_14, F_14  }, //3
    { 0, D_13, E_13, F_13  }, //4
    { 0, D_12, E_12, F_12  }, //5
    { 0, D_11, E_11, F_11  }, //6
    { 0, D_10, E_10, F_10  }, //7
    { 0, D_9,  E_9,  F_9   }, //8
    { 0, D_8,  E_8,  F_8   }, //9
    { 0, D_7,  E_7,  F_7   }, //10
    { 0, D_6,  E_6,  F_6   }, //11
    { 0, D_5,  E_5,  F_5   }, //12
    { 0, D_4,  E_4,  F_4   }, //13
    { 0, D_3,  E_3,  F_3   }, //14
    { 0, D_2,  E_2,  F_2   }, //15
    { 0, D_1,  E_1,  F_1   }, //16
    { 0, A_10, B_10, C_10  }, //17
    { 0, A_9,  B_9,  C_9   }, //18
    { 0, A_8,  B_8,  C_8   }, //19
    { 0, A_7,  B_7,  C_7   }, //20
    { 0, A_15, B_15, C_15  }, //21
    { 0, A_14, B_14, C_14  }, //22
    { 0, A_13, B_13, C_13  }, //23
    { 0, A_12, B_12, C_12  }, //24
    { 0, A_11, B_11, C_11  }, //25
    { 0, A_16, B_16, C_16  }, //26
    { 0, J_16, K_16, L_16  }  //27
};

led_config_t g_led_config = {
    {
        {  0,  1, 2,  3,  4,  5},
        {  6,  7, 8,  9,  10,  11},
        {  12,  13, 14,  15,  16,  17},
        {  18,  19, 20,  21,  22,  23},
        {  24,  25, 26,  27,  27,  NO_LED},
        {  0,  0, 0,  0,  0,  0}
    }, {
        { 0, 64 }, { 1, 64 }, { 2, 64 }, { 3, 64 }, { 64, 64 }, 
		{ 0, 64 }, { 1, 64 }, { 2, 64 }, { 3, 64 }, { 64, 64 }, 
		{ 0, 64 }, { 1, 64 }, { 2, 64 }, { 3, 64 }, { 64, 64 }, 
		{ 0, 64 }, { 1, 64 }, { 2, 64 }, { 3, 64 }, { 64, 64 },
		{ 0, 64 }, { 1, 64 }, { 2, 64 }, { 3, 64 }, { 64, 64 },
		{ 0, 64 }, { 1, 64 }, { 2, 64 }
    }, {
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1
    }
};
